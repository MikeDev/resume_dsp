\contentsline {chapter}{\numberline {1}Introduction to security}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Types of security}{8}{section.1.1}
\contentsline {section}{\numberline {1.2}Attacks}{8}{section.1.2}
\contentsline {section}{\numberline {1.3}Techniques to prevent or detect the attacks}{8}{section.1.3}
\contentsline {chapter}{\numberline {2}Shared Key}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}A formal definition of shared key encryption}{10}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Proprieties of decryption and encryption functions}{10}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Cryptanalysis}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Power of the attacker}{11}{section.2.3}
\contentsline {chapter}{\numberline {3}Classic cypher techniques}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Shift encryption}{14}{section.3.1}
\contentsline {section}{\numberline {3.2}Cifrari a sostituzione monoalfabetici}{14}{section.3.2}
\contentsline {section}{\numberline {3.3}Cifrari a blocchi}{14}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Cifrario di Hill}{14}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Proposizione (Invertibilita')}{15}{section.3.4}
\contentsline {section}{\numberline {3.5}Cifrari polialfabetici}{15}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Cifrario di Vigenere'}{15}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Attacco al cifrario di Vigenere'}{16}{subsection.3.5.2}
\contentsline {subsubsection}{Test di Kasiski}{16}{section*.2}
\contentsline {subsubsection}{Indici di coincidenza}{16}{section*.3}
\contentsline {subsubsection}{Determinazione della chiave}{17}{section*.4}
\contentsline {chapter}{\numberline {4}Perfect Secrecy e One-Time Pad}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Cifrari perfetti secondo Shannon}{20}{section.4.1}
\contentsline {section}{\numberline {4.2}Proposizione}{20}{section.4.2}
\contentsline {subsubsection}{Dimostrazione}{20}{section*.5}
\contentsline {section}{\numberline {4.3}One time pad}{20}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}One time pad e' un cifrario perfetto}{20}{subsection.4.3.1}
\contentsline {subsubsection}{Dimostrazione}{20}{section*.6}
\contentsline {subsection}{\numberline {4.3.2}Sulla praticita' di OTP}{21}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}SPN E cifrari di feistel}{23}{chapter.5}
\contentsline {section}{\numberline {5.1}Reti di Sostituzione-Permutazione}{24}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Diffusione}{24}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Confusione}{24}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Cifrari di Feistel}{24}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Encryption}{24}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Decryption}{25}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Parametri fondamentali dei cifrari di Feistel}{26}{subsection.5.2.3}
\contentsline {subsubsection}{Criteri per le S-box}{26}{section*.7}
\contentsline {chapter}{\numberline {6}Gestione della Chiave Condivisa}{27}{chapter.6}
\contentsline {section}{\numberline {6.1}Link encryption vs End-to-End encryption}{28}{section.6.1}
\contentsline {section}{\numberline {6.2}Pro e contro di Link encryption e End-to-End encryption}{28}{section.6.2}
\contentsline {section}{\numberline {6.3}Distribuzione della chiave segreta tramite Key Distribution Center (KDC)}{28}{section.6.3}
\contentsline {section}{\numberline {6.4}Durata della chiave di sessione}{29}{section.6.4}
\contentsline {chapter}{\numberline {7}Chiave pubblica}{31}{chapter.7}
\contentsline {section}{\numberline {7.1}Chiave pubblica e privata}{32}{section.7.1}
\contentsline {section}{\numberline {7.2}Proprieta' delle chiavi private e pubbliche}{32}{section.7.2}
\contentsline {section}{\numberline {7.3}One way}{32}{section.7.3}
\contentsline {subsubsection}{Alcuni esempi di funzioni one way}{32}{section*.8}
\contentsline {section}{\numberline {7.4}Chiave pubblica e privata applicata al KDC}{32}{section.7.4}
\contentsline {section}{\numberline {7.5}Firma digitale}{33}{section.7.5}
\contentsline {chapter}{\numberline {8}Aritmetica modulare}{35}{chapter.8}
\contentsline {section}{\numberline {8.1}Teorema della divisione}{36}{section.8.1}
\contentsline {section}{\numberline {8.2}Coprimi}{36}{section.8.2}
\contentsline {section}{\numberline {8.3}Congruo modulo n}{36}{section.8.3}
\contentsline {section}{\numberline {8.4}Inverso modulo n}{36}{section.8.4}
\contentsline {section}{\numberline {8.5}Operazioni modulo n}{36}{section.8.5}
\contentsline {section}{\numberline {8.6}Algoritmo di euclide}{36}{section.8.6}
\contentsline {subsection}{\numberline {8.6.1}Dimostrazione}{37}{subsection.8.6.1}
\contentsline {section}{\numberline {8.7}Identita di Bezout}{37}{section.8.7}
\contentsline {subsubsection}{Dimostrazione}{37}{section*.9}
\contentsline {section}{\numberline {8.8}Esistenza ed unicita' dell'inverso di a mod n}{37}{section.8.8}
\contentsline {subsubsection}{Dimostrazione}{37}{section*.10}
\contentsline {section}{\numberline {8.9}Piccolo teorema di Fermat}{37}{section.8.9}
\contentsline {subsubsection}{Dimostrazione}{37}{section*.11}
\contentsline {section}{\numberline {8.10}La funzione $ \phi (n) $}{38}{section.8.10}
\contentsline {section}{\numberline {8.11}Teorema di Eulero}{38}{section.8.11}
\contentsline {subsubsection}{Dimostrazione}{38}{section*.12}
\contentsline {section}{\numberline {8.12}Base di RSA}{39}{section.8.12}
\contentsline {subsubsection}{Dimostrazione}{39}{section*.13}
\contentsline {subsection}{\numberline {8.12.1}Alcune note su RSA}{40}{subsection.8.12.1}
\contentsline {section}{\numberline {8.13}Algoritmo di esponenziazione veloce}{40}{section.8.13}
\contentsline {section}{\numberline {8.14}Teorema cinese del resto}{40}{section.8.14}
\contentsline {subsubsection}{Dimostrazione}{40}{section*.14}
\contentsline {section}{\numberline {8.15}Generazione dei numeri primi}{41}{section.8.15}
\contentsline {section}{\numberline {8.16}Test di primalita'}{42}{section.8.16}
\contentsline {section}{\numberline {8.17}Test di Fermat}{42}{section.8.17}
\contentsline {subsubsection}{Testimoni di Fermat e numeri di Charmicheal}{42}{section*.15}
\contentsline {section}{\numberline {8.18}Test di Rabin}{42}{section.8.18}
\contentsline {subsubsection}{Proposizione}{42}{section*.16}
\contentsline {subsubsection}{Dimostrazione}{42}{section*.17}
\contentsline {section}{\numberline {8.19}Radici primitive}{43}{section.8.19}
\contentsline {subsection}{\numberline {8.19.1}Teorema}{44}{subsection.8.19.1}
\contentsline {section}{\numberline {8.20}Funzione di esponenziazione e logaritmo discreto}{44}{section.8.20}
\contentsline {chapter}{\numberline {9}RSA}{45}{chapter.9}
\contentsline {section}{\numberline {9.1}Parametri di RSA}{46}{section.9.1}
\contentsline {section}{\numberline {9.2}Encryption e decryption}{46}{section.9.2}
\contentsline {section}{\numberline {9.3}Aspetti computazionali}{46}{section.9.3}
\contentsline {section}{\numberline {9.4}Sicurezza di RSA}{46}{section.9.4}
\contentsline {subsection}{\numberline {9.4.1}Forza bruta}{46}{subsection.9.4.1}
\contentsline {subsection}{\numberline {9.4.2}Attacchi matematici}{46}{subsection.9.4.2}
\contentsline {subsection}{\numberline {9.4.3}Dimostrazione equivalenza computazionale fra i precedenti problemi}{47}{subsection.9.4.3}
\contentsline {subsection}{\numberline {9.4.4}Attacchi chosen plaintext}{47}{subsection.9.4.4}
\contentsline {chapter}{\numberline {10}El Gamal}{49}{chapter.10}
\contentsline {section}{\numberline {10.1}Chiave pubblica e privata}{50}{section.10.1}
\contentsline {section}{\numberline {10.2}Encryption e decryption}{50}{section.10.2}
\contentsline {section}{\numberline {10.3}Firma digitale con El Gamal (El gamal signature)}{50}{section.10.3}
\contentsline {section}{\numberline {10.4}Scambio della chiave di Diffie-Hellman}{50}{section.10.4}
\contentsline {chapter}{\numberline {11}Funzioni di autenticazione}{53}{chapter.11}
\contentsline {section}{\numberline {11.1}Funzioni hash crittografiche}{54}{section.11.1}
\contentsline {section}{\numberline {11.2}Proprieta' delle funzioni hash crittografiche}{54}{section.11.2}
\contentsline {section}{\numberline {11.3}Schema di Merkle-Dangard}{54}{section.11.3}
\contentsline {section}{\numberline {11.4}Attacco del compleanno}{55}{section.11.4}
\contentsline {section}{\numberline {11.5}MAC}{55}{section.11.5}
\contentsline {chapter}{\numberline {12}Teoria dell'informazione}{57}{chapter.12}
\contentsline {section}{\numberline {12.1}Entropia}{58}{section.12.1}
\contentsline {section}{\numberline {12.2}Entropia condizionale}{58}{section.12.2}
\contentsline {section}{\numberline {12.3}Mutua informazione}{58}{section.12.3}
\contentsline {section}{\numberline {12.4}Divergenza di Kullback-Leibler}{58}{section.12.4}
\contentsline {subsection}{\numberline {12.4.1}Applicazione nel test d'ipotesi e lemma di Chernoff-Stein}{58}{subsection.12.4.1}
\contentsline {subsection}{\numberline {12.4.2}Applicazione nella compressione dati}{59}{subsection.12.4.2}
\contentsline {section}{\numberline {12.5}Linking Identity}{59}{section.12.5}
\contentsline {subsection}{\numberline {12.5.1}Dimostrazione}{59}{subsection.12.5.1}
\contentsline {section}{\numberline {12.6}Diseguaglianza di Gibbs}{59}{section.12.6}
\contentsline {section}{\numberline {12.7}Proprieta' dell'entropia}{59}{section.12.7}
\contentsline {subsection}{\numberline {12.7.1}Chain rule}{59}{subsection.12.7.1}
\contentsline {subsection}{\numberline {12.7.2}Corollario}{59}{subsection.12.7.2}
\contentsline {chapter}{\numberline {13}Compressione dati}{61}{chapter.13}
\contentsline {section}{\numberline {13.1}Problema della compressione}{62}{section.13.1}
\contentsline {section}{\numberline {13.2}Univocamente decodificabile}{62}{section.13.2}
\contentsline {section}{\numberline {13.3}Prefix free (instantaneo)}{62}{section.13.3}
\contentsline {subsubsection}{Osservazione}{62}{section*.21}
\contentsline {section}{\numberline {13.4}Lemma di Kraft}{62}{section.13.4}
\contentsline {section}{\numberline {13.5}Lemma di McMillan}{62}{section.13.5}
\contentsline {section}{\numberline {13.6}Proposizione}{62}{section.13.6}
\contentsline {subsubsection}{Dimostrazione}{63}{section*.22}
\contentsline {section}{\numberline {13.7}Codici di compressione ottimi}{63}{section.13.7}
\contentsline {subsection}{\numberline {13.7.1}Ridondanza di un codice}{63}{subsection.13.7.1}
\contentsline {section}{\numberline {13.8}Proposizione}{63}{section.13.8}
\contentsline {subsubsection}{Dimostrazione}{63}{section*.23}
\contentsline {section}{\numberline {13.9}Codici di Huffman}{64}{section.13.9}
\contentsline {chapter}{\numberline {14}Canali e capacita'}{67}{chapter.14}
\contentsline {section}{\numberline {14.1}Canali}{68}{section.14.1}
\contentsline {section}{\numberline {14.2}Capacita}{68}{section.14.2}
\contentsline {section}{\numberline {14.3}Simmetria debole}{68}{section.14.3}
\contentsline {section}{\numberline {14.4}Ridondanza}{69}{section.14.4}
\contentsline {subsection}{\numberline {14.4.1}Esempio}{69}{subsection.14.4.1}
\contentsline {subsection}{\numberline {14.4.2}(M,n)-codice}{69}{subsection.14.4.2}
\contentsline {section}{\numberline {14.5}Rate}{69}{section.14.5}
\contentsline {section}{\numberline {14.6}Teorema di channel coding di Shannon}{69}{section.14.6}
\contentsline {chapter}{\numberline {15}Privacy}{71}{chapter.15}
\contentsline {section}{\numberline {15.1}Tipi di attributi secondo la privacy}{72}{section.15.1}
\contentsline {section}{\numberline {15.2}Attaccante}{72}{section.15.2}
\contentsline {section}{\numberline {15.3}k-Anonimity}{72}{section.15.3}
\contentsline {section}{\numberline {15.4}l-diversity}{72}{section.15.4}
\contentsline {subsection}{\numberline {15.4.1}Bayes optimal privacy}{73}{subsection.15.4.1}
\contentsline {subsection}{\numberline {15.4.2}Positive disclosure}{73}{subsection.15.4.2}
\contentsline {subsection}{\numberline {15.4.3}Negative disclosure}{73}{subsection.15.4.3}
\contentsline {subsection}{\numberline {15.4.4}Misure della Bayes optimal privacy}{74}{subsection.15.4.4}
\contentsline {subsection}{\numberline {15.4.5}Limiti della Bayes optimal privacy}{74}{subsection.15.4.5}
\contentsline {section}{\numberline {15.5}Teorema sulla distribuzione a posteriori della l-diversity}{74}{section.15.5}
\contentsline {subsection}{\numberline {15.5.1}Positive disclosure applicato al teorema}{74}{subsection.15.5.1}
\contentsline {section}{\numberline {15.6}Blocco well represented}{74}{section.15.6}
\contentsline {section}{\numberline {15.7}l-diversity ricorsiva}{75}{section.15.7}
\contentsline {section}{\numberline {15.8}Come realizzare la l-diversity}{75}{section.15.8}
\contentsline {section}{\numberline {15.9}Sparsita'}{75}{section.15.9}
\contentsline {section}{\numberline {15.10}De-anonimizzazione}{75}{section.15.10}
\contentsline {subsection}{\numberline {15.10.1}Definizioni}{75}{subsection.15.10.1}
\contentsline {subsection}{\numberline {15.10.2}Algoritmo per la de-anonimizzazione}{76}{subsection.15.10.2}
\contentsline {subsection}{\numberline {15.10.3}Teorema 1}{76}{subsection.15.10.3}
\contentsline {subsubsection}{Idea della dimostrazione}{76}{section*.24}
\contentsline {subsection}{\numberline {15.10.4}False match e lemma}{76}{subsection.15.10.4}
\contentsline {subsubsection}{Dimostrazione}{76}{section*.25}
\contentsline {subsection}{\numberline {15.10.5}Teorema 2 (Teorema 1 sfruttando la sparsita')}{76}{subsection.15.10.5}
\contentsline {subsubsection}{Dimostrazione}{76}{section*.26}
\contentsline {subsection}{\numberline {15.10.6}Esempio Netflix}{76}{subsection.15.10.6}
\contentsline {section}{\numberline {15.11}Privacy differenziale}{77}{section.15.11}
\contentsline {subsection}{\numberline {15.11.1}Meccanismo laplaciano per la privacy}{77}{subsection.15.11.1}
\contentsline {subsubsection}{Global sensitivity}{78}{section*.27}
\contentsline {subsection}{\numberline {15.11.2}Relaxed semantic privacy}{78}{subsection.15.11.2}
\contentsline {chapter}{\numberline {16}Sicurezza incondizionata}{79}{chapter.16}
\contentsline {section}{\numberline {16.1}Cifrario secondo in base all'entropia}{80}{section.16.1}
\contentsline {subsection}{\numberline {16.1.1}Key equivocation e plain text equivocation}{80}{subsection.16.1.1}
\contentsline {section}{\numberline {16.2}Proposizione}{80}{section.16.2}
\contentsline {subsubsection}{Dimostrazione}{80}{section*.28}
\contentsline {section}{\numberline {16.3}Proposizione 2}{80}{section.16.3}
\contentsline {subsubsection}{Dimostrazione}{81}{section*.29}
\contentsline {section}{\numberline {16.4}Unicity distance}{81}{section.16.4}
